package students;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Students {
    private final String firstname, lastname;

    public Students(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;

    }


    public void print() {
        System.out.printf("Name: %s %s%n", firstname, lastname);
    }

    public static void printEnd() {
        try {
            File end = new File("src\\students\\ende.txt");
            Scanner scanner = new Scanner(end);
            while (scanner.hasNext()) {
                System.out.println
                        (scanner.nextLine());
            }

        } catch (FileNotFoundException e){
            System.out.println("File not found!");
        }
    }
}
