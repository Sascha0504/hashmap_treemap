package main;

import students.Students;

import java.security.Key;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import static java.util.Objects.hash;

public class Main {
    public static void main(String[] args) {
        // create arrays with names for Name generation for Maps
        String[] firstnames = {"Max", "Anakin", "Biene", "Hans", "Pippi"};
        String[] lastnames = {"Muster", "Skywalker", "Maja", "Mayer", "Langstrumpf"};
        int counter = 0;

        //create hashmap and treemap
        HashMap<String, Students> testHash = new HashMap<>();
        TreeMap<String, Students> testTree = new TreeMap<>();


        //add objects to the maps
        for (String fname : firstnames) {
            for (String lname : lastnames) {
                testHash.put("student" + counter, new Students(fname, lname));
                testTree.put("student" + counter, new Students(fname, lname));
                counter++;
            }
        }

        //remove 1 item of the map
        testHash.remove("student1");

        //iterate through both maps with keys, entrys and values
//        for(Map.Entry<String, Students> entry : testHash.entrySet()){
//            //System.out.println(entry.getKey());
//            entry.getValue().print();
//        }

//        for (String key : testTree.keySet()){
//            testTree.get(key).print();
//        }

//        for (Students student : testHash.values()){
//            student.print();
//        }

        //clears
        testHash.clear();
        System.out.println(testHash.size());

        Students.printEnd();
    }
}
